project(
  'toggle',
  version: '45.0-1',
  license: 'GPL3',
  meson_version: '>= 0.63.0',
)

# Set profile to a clean name
if get_option('profile') == 'development'
  profile = 'Devel'
else
  profile = ''
endif

# Set profile suffix
# Production builds don't have a suffix
if profile != ''
  profile_suffix = '.' + profile
else
  profile_suffix = ''
endif

# Set application id based on the profile
application_id = 'app.drey.Toggle' + profile_suffix

gettext_package = application_id
prefix = get_option('prefix')
bindir = prefix / get_option('bindir')
libdir = prefix / get_option('libdir')
datadir = prefix / get_option('datadir')
pkgdatadir = datadir / application_id

schemadir = datadir / 'glib-2.0' / 'schemas'

po_dir = meson.project_source_root() / 'po'

gjs_dep = dependency('gjs-1.0', version: '>= 1.54.0')
gjs_console = gjs_dep.get_variable(pkgconfig: 'gjs_console')

# Verify that the dependencies exist
dependency('gio-2.0', version: '>= 2.43.4')
dependency('glib-2.0', version: '>= 2.39.3')
dependency('gtk4', version: '>= 4.4.0')
dependency('libadwaita-1', version: '>=  1.5.0')
dependency('gobject-introspection-1.0', version: '>= 1.31.6')


gnome = import('gnome')
i18n = import('i18n')

tsc = find_program('tsc', required: true)

subdir('data')
subdir('src')
subdir('po')


install_symlink(
  meson.project_name(),
  pointing_to: pkgdatadir / application_id,
  install_dir: bindir
)

gnome.post_install(
  glib_compile_schemas: true,
  gtk_update_icon_cache: true,
  update_desktop_database: true,
)
