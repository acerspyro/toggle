# Toggle

For an overview of the features we plan to keep from Tweaks and move to Settings, see FEATURES.md.

To start contributing to Toggle, see HACKING.md.

We have a Matrix channel! Come join us over at [`#toggle-application:fedora.im`](https://matrix.to/#/#toggle-application:fedora.im)

## TODO

After *all* of the following is checked off, we can release the first stable build of Toggle. We'll be following a GNOME-like release schedule, but with more minor updates for things like releases.

The actual release schedule is TBD; we can go with semantic versioning, GNOME-like versioning, or date-based versioning (YYYY.MM.DD)

- [ ] Add translations support, apply for free Weblate
- [ ] Add info button capabilities - See Settings -> Mouse & Touchpad -> Mouse Acceleration for reference
- [ ] Refuse to run outside of a GNOME session
- [ ] Lock out certain options when the current GNOME version doesn't contain nor support them
- [ ] Set up CI
- [ ] Set up sane formatting (two space indents)
  - [ ] Automate, with in-Flatpak-extension linters ran occasionally in CI, with pull requests
- [ ] Update screenshots when ready for release
- [ ] Verify `start-toggle.sh` script
- [ ] Implement the tweaks
  - [ ] Appearance
    - [ ] Fonts
    - [ ] Themes
  - [ ] Desktop
    - [ ] Window action key
      - [ ] Left Super
      - [ ] Right Super
    - [ ] Headerbar actions
      - [ ] Make sure the code is optimized and reusable for all three dropdowns
      - [ ] Convert to a dialog
    - [x] Button layout
      - [ ] Make sure behavior is consistent in Start and End configurations
      - [ ] Convert to a dialog
    - [x] Fractional scaling experimental feature
    - [x] Light Style
      - [ ] Warn that it's only partially available in GNOME 45+, and is still WIP
  - [ ] Devices
    - [ ] Suspend when laptop lid is closed
  - [ ] Startup Apps
    - Can probably reuse the dconf hack for getting the host XDG_DATA_DIRS to get access to the host desktop files and icons
    - [ ] Access `org.freedesktop.impl.portal.gnome` for setting and reading background app state
    - [ ] Write helpers for the former
    - [ ] Add search menu for apps
- [x] Make an icon
- [x] Add a debug info function
- [x] Add Blueprint support to `data/ui`

## Mockup

![The mockup for Toggle, giving an overview of all the pages and subpages containing preferences](mockup/toggle.png)

## Motivation

GNOME Tweaks is barely maintained, many of the settings are broken in newer GNOME versions, and many of the settings have already been moved to GNOME Settings. Since many users still rely on a couple of settings that Tweaks provides, we need a solution.

## Goal

Our goal is to completely replace GNOME Tweaks with a modernized experience. This includes:

- Cleaned up settings, nothing that doesn't work / is already implemented in GNOME Settings
- A new UI, made with GTK4 and Libadwaita
- Shipped as a Flatpak, hosted on Flathub

Non-Flatpak builds of Toggle will **not** be supported. Issues that can not be reproduced on Flatpak builds of Toggle will be closed, and issues from native packages will be closed immediately, without question.
