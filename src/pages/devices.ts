import Adw from "gi://Adw";
import Gio from "gi://Gio";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import {
  bind_setting,
  bind_setting_array,
  bind_setting_enums,
  bind_setting_map,
  bind_setting_string,
  get_enum_map,
  get_settings,
} from "./util.js";

export class ToggleDevicesPreferencePage extends Adw.PreferencesPage {
  private _suspend_lid_closed!: Adw.SwitchRow;
  private _palm_rejection!: Adw.SwitchRow;
  private _middle_click_paste!: Adw.SwitchRow;
  private _overview_shortcut!: Adw.ComboRow;
  private _mouse_click_fingers!: Gtk.CheckButton;
  private _mouse_click_area!: Gtk.CheckButton;
  private _mouse_click_disabled!: Gtk.CheckButton;

  static {
    GObject.registerClass(
      {
        GTypeName: "ToggleDevicesPreferencePage",
        Template: "resource:///io/gitlab/orowith2os/Toggle/ui/pages/devices.ui",
        InternalChildren: [
          "suspend_lid_closed",
          "palm_rejection",
          "middle_click_paste",
          "overview_shortcut",
          "mouse_click_fingers",
          "mouse_click_area",
          "mouse_click_disabled",
        ],
        Signals: {
          warning_accepted: {},
        },
      },
      this,
    );
  }

  constructor(params?: Partial<Adw.PreferencesPage.ConstructorProperties>) {
    super(params);

    bind_setting(
      "org.gnome.desktop.peripherals.touchpad",
      "disable-while-typing",
      this._palm_rejection,
    );

    bind_setting(
      "org.gnome.desktop.interface",
      "gtk-enable-primary-paste",
      this._middle_click_paste,
    );

    bind_setting_array(
      "org.gnome.mutter",
      "overlay-key",
      this._overview_shortcut,
      "selected",
      ["Super_L", "Super_R"]
    )

    bind_setting_enums(
      "org.gnome.desktop.peripherals.touchpad",
      "click-method",
      [
        [ this._mouse_click_fingers, "active", "fingers" ],
        [ this._mouse_click_area, "active", "areas" ],
        [ this._mouse_click_disabled, "active", "none" ]
      ]
    );
  }
}
