import Adw from "gi://Adw";
import GObject from "gi://GObject";

export class ToggleStartupAppsPreferencePage extends Adw.PreferencesPage {

  static {
    GObject.registerClass(
      {
        GTypeName: "ToggleStartupAppsPreferencePage",
        Template: "resource:///io/gitlab/orowith2os/Toggle/ui/pages/startup-apps.ui",
      },
      this,
    );
  }

  constructor(params?: Partial<Adw.PreferencesPage.ConstructorProperties>) {
    super(params);
  }
}