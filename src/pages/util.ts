import Adw from "gi://Adw";
import GObject from "gi://GObject";
import Gio from "gi://Gio";
import Gtk from "gi://Gtk?version=4.0";

type NumberKeys<T> = {
  [K in keyof T]: T[K] extends number ? K : never;
}[keyof T];

/**
 * Gets a GSetting with a given name
 * @param path
 * @returns the GSetting or `null` if the setting was not found
 */
export function get_settings(path: string): Gio.Settings | null {
  try {
    return Gio.Settings.new(path);
  } catch (e) {
    console.error(`Can't get GSetting: ${e}`);
    return null;
  }
}

/**
 * Binds a given GSetting key to a widget. Whenever the GSetting key
 * changes, the widget's state gets updated and vice-versa
 * @param path The path to the GSetting
 * @param key The key of the setting
 * @param widget The widget to bind the setting to
 * @param flags `Gio.SettingsBindFlags` that modify the binding
 */
export function bind_setting(
  path: string,
  key: string,
  widget: Adw.SwitchRow | Gtk.ToggleButton,
  flags = Gio.SettingsBindFlags.DEFAULT,
) {
  const setting = get_settings(path);

  if (!setting) return;

  if (widget instanceof Adw.SwitchRow || widget instanceof Gtk.ToggleButton) {
    setting.bind(key, widget, "active", flags);
  }
}

/**
 * A function that is called everytime the GSetting changes and the widget needs
 * to get updated
 */
export type BindSettingFromCallback<Widget extends Gtk.Widget> = (
  settings: Gio.Settings,
  widget: Widget,
) => void;

/**
 * A function that is called every time a widget's property changes and the
 * correct GSetting needs to be saved
 */
export type BindSettingToCallback<Widget extends Gtk.Widget> = (
  settings: Gio.Settings,
  widget: Widget,
) => void;

// this is a hack because GJS doesn't support `Gio.Settings.bind_with_mapping`

/**
 * Bind a `GSetting` to a `Gtk.Widget`.
 * This functions creates a "binding" i.e calls the given callback when a
 * GSetting key is updated to update a widget and calls another callback when
 * a widget's property is changed to update the GSetting. Basically a
 * re-implementation of `Gio.Settings.bind_with_mapping` because that function
 * is not supported in language bindings.
 * @param path The path of the GSetting to bind to
 * @param key The key of the setting
 * @param widget The widget that needs to get binded to the setting
 * @param property The property of the widget that will get updated when the
 * setting changes and vice-versa.
 * @param from The function that will get called when the setting gets changed
 * (in GSettings) and the widget needs to get updated
 * @param to The function that will get called when the widget's property
 * changes and the new setting needs to get stored
 */
export function bind_setting_map<Widget extends Gtk.Widget>(
  path: string,
  key: string,
  widget: Widget,
  property: string,
  from: BindSettingFromCallback<Widget> | null,
  to: BindSettingToCallback<Widget>,
) {
  const settings = get_settings(path);

  if (!settings) return;

  // This function is called whenever the setting changes and the widget needs
  // to get updated
  function fire_from() {
    from?.(settings!, widget);
  }

  // This function is called when a widget's property changes, and the GSetting
  // needs to get stored
  function fire_to() {
    to?.(settings!, widget);
  }

  // Initially, we want to set the widget's state according to the value of
  // the GSetting
  fire_from();

  settings.connect(`changed::${key}`, fire_from);
  widget.connect(`notify::${property}`, fire_to);

  // Clean up the signals when the widget is unrealized
  widget.connect("unrealize", () => {
    GObject.signal_handlers_disconnect_by_func(settings, fire_from);
    GObject.signal_handlers_disconnect_by_func(widget, fire_to);
  });
}

/**
 * A convenience function that allows to bind setting keys that are enums.
 * The widget's property will be set to true if the setting key enum has the
 * value named (`enum_string`), and false if it has the value named
 * (`enum_disabled_string`). For more information, see `bind_setting_map`
 */
export function bind_setting_enum<Widget extends Gtk.Widget>(
  path: string,
  key: string,
  widget: Widget,
  property: keyof Widget,
  enum_string: string,
  enum_disabled_string: string | undefined
) {
  // get the enum map of the key.
  const enum_map = get_enum_map(path, key);
  if (!enum_map) return;

  // gets the corresponding index number of the enum name (`enum_string`)
  const enum_value = enum_map[enum_string];
  if (!enum_value) {
    console.error(
      `enum \`${path}:${key}\` doesn't have a value named \`${enum_string}\``,
    );
    return;
  }

  let enum_second_value: number | undefined;
  if (enum_disabled_string) {
    enum_second_value = enum_map[enum_disabled_string];

    if (enum_second_value == undefined) {
      console.error(
        `enum \`${path}:${key}\` doesn't have a value named \`${enum_disabled_string}\``,
      );
      return;
    }
  }

  bind_setting_map(
    path,
    key,
    widget,
    property as string,
    (settings, widget) => {
      // The widget's property is set to true if the setting's value is equal
      // to the enum value (corresponding to the passed `enum_string`).
      widget[property] = (settings.get_enum(key) === enum_value) as any;
    },
    (settings, widget) => {
      if (widget[property]) {
        // If the widget's property is set to true, update the setting to the
        // enum value.
        settings.set_enum(key, enum_value);
      } else if (enum_second_value !== undefined) {
        settings.set_enum(key, enum_second_value);
      }
    },
  );
}

/**
 * A convenience function that allows to bind multiple setting keys that are
 * enums. The widget's property will be set to true if the setting key enum has the
 * value named (`enum_string`) or false otherwise. For more information, see
 * `bind_setting_map`
 */
export function bind_setting_enums<Widget extends Gtk.Widget>(
  path: string,
  key: string,
  actions: [ Widget, keyof Widget, string, string? ][]
) {
    for (const [ widget, property, enum_string, enum_disabled_string ] of actions) {
      bind_setting_enum(
      	path,
      	key,
      	widget,
      	property,
      	enum_string,
      	enum_disabled_string
      )
    }
}

type GioDatatypes = "boolean"
  | "double"
  | "enum"
  | "flags"
  | "int"
  | "int64"
  | "string"
  | "strv"
  | "uint"
  | "uint64"
  | "value";

/**
 * A convenience function that allows to bind setting keys that are arrays.
 * @param path The path of the GSetting to bind to
 * @param key The key of the setting
 * @param widget The widget that needs to get binded to the setting
 * @param property The property of the widget that will get updated when the
 * setting changes and vice-versa.
 * @param dictionnary An array of strings that will be used to map the setting
 * value to the widget's property value. The widget's property will be set to
 * the index of the setting's value in the array.
 */
export function bind_setting_array<Widget extends Gtk.Widget>(
  path: string,
  key: string,
  widget: Widget,
  property: NumberKeys<Widget>,
  dictionnary: Array<any>,
  dataType: GioDatatypes = "string"
) {
  bind_setting_map(
    path,
    key,
    widget,
    property as string,
    (settings, widget) => {
      const settingsValue = settings[`get_${dataType}`](key);
      widget[property] = dictionnary.findIndex((value) => settingsValue == value) as any;
    },
    (settings, widget) => {
      settings[`set_${dataType}`](key, dictionnary[widget[property] as number] as never);
    },
  )
}

/**
 * A convenience function to bind a GSetting key that is a string. If the
 * setting's value is exactly equal to `value`, The widget's property will be
 * set to true, or false otherwise. The setting will also be set to `value` if
 * the widget's property is changed to `true`. For more information, see
 * `bind_setting_map`
 */
export function bind_setting_string<Widget extends Gtk.Widget>(
  path: string,
  key: string,
  widget: Widget,
  property: keyof Widget,
  value: string,
  disabled_value?: string
) {
  bind_setting_map(
    path,
    key,
    widget,
    property as string,
    (settings, widget) => {
      widget[property] = (settings.get_string(key) === value) as any;
    },
    (settings, widget) => {
      if (widget[property]) {
        settings.set_string(key, value);
      }
      if (!widget[property] && disabled_value) {
        settings.set_string(key, disabled_value)
      }
    },
  );
}

/**
 * A convenience function that allows to bind setting keys that are flags.
 * The widget's property will be set to true if the setting key flags includes
 * the flag named `flag_string` or false otherwise. For more information, see
 * `bind_setting_map`
 */
export function bind_setting_flag<Widget extends Gtk.Widget>(
  path: string,
  key: string,
  widget: Widget,
  property: keyof Widget,
  flag_string: string,
) {
  // get the flag map of the key.
  const flag_map = get_flags_map(path, key);

  if (!flag_map) return;

  // gets the corresponding index number of the flag name (`flag_string`)
  const flag_value = flag_map[flag_string];

  if (!flag_value) {
    console.error(
      `flags \`${path}:${key}\` doesn't have a value named \`${flag_string}\``,
    );
    return;
  }

  bind_setting_map(
    path,
    key,
    widget,
    property as string,
    (settings, widget) => {
      // The widget's property is set to true if the setting's value (which are
      // flags) includes the given flag.
      widget[property] = ((settings.get_flags(key) & flag_value) != 0) as any;
    },
    (settings, widget) => {
      // Get the setting's value (as flags)
      const flags = settings.get_flags(key);

      const modified = widget[property]
        // If the widget's property is true, add the flag to the list of flags
        ? flags | flag_value
        // If the widget's property is false, remove the flag from the list of
        // flags
        : flags & ~flag_value;

      // Update the setting
      settings.set_flags(key, modified);
    },
  );
}

/**
 * Returns an object that maps enum names (strings) to enum indices (numbers).
 * Because GSettings stores enums as numbers, we need to get what number a given
 * enum value (which is a string) corresponds to. This allows us to only
 * remember the enum names and not the corresponding indices.
 * @param path The path of the GSetting
 * @param key The key of the setting
 * @returns An object that maps enum names to enum indices or null if the
 * given setting could not be found
 */
export function get_enum_map(
  path: string,
  key: string,
): Record<string, number> | null {
  const settings = get_settings(path);

  if (!settings) return null;

  // Gets the type of the setting key
  const range = settings.settings_schema.get_key(key)?.get_range();

  if (range) {
    // setting key schemas have a `range` of type `GVariant` with the `(sv)`
    // signature. This means that the range of a key is a tuple with the first
    // value being a string and the second being a variant. For more information
    // see the docs for `Gio.SettingsSchemaKey.get_range`. Here we are querying
    // the first value (type) as a string.
    const type_str = range.get_child_value(0).get_string()[0];

    // We are only interested in keys which are enums
    if (type_str === "enum") {
      // Get the second value of the range. It's a variant containing an array.
      const array = range.get_child_value(1).get_variant();
      const obj: Record<string, number> = {};

      // loop over the array's children. The array contains a list of all the
      // enum values as strings. The index (`i`) represents the enum indices.
      for (let i = 0; i < array.n_children(); i++) {
        const element = array.get_child_value(i);

        obj[element.get_string()[0]] = i;
      }

      return obj;
    }
  }

  return null;
}

/**
 * Returns an object that maps flag names (strings) to flag indices (numbers).
 * Because GSettings stores flags as numbers, we need to get what number a given
 * flag value (which is a string) corresponds to. This allows us to only
 * remember the flag names and not the corresponding numbers.
 * @param path The path of the GSetting
 * @param key The key of the setting
 * @returns An object that maps flag names to flag indices or null if the
 * given setting could not be found
 */
export function get_flags_map(
  path: string,
  key: string,
): Record<string, number> | null {
  const settings = get_settings(path);

  if (!settings) return null;

  // Gets the range of the key. The range has a signature of `(sv)`
  // meaning the first child is a string, and the second is a variant.
  const range = settings.settings_schema.get_key(key)?.get_range();

  if (range) {
    const type_str = range.get_child_value(0).get_string()[0];

    // We are only interested in settings that are flags
    if (type_str === "flags") {
      // Gets the second child of the `range`, which is a variant containing
      // an array
      const array = range.get_child_value(1).get_variant();
      const obj: Record<string, number> = {};

      // loop over the array's children. The array contains a list of all the
      // flag values as strings. The index (`i`) represents the flag indices.
      for (let i = 0; i < array.n_children(); i++) {
        const element = array.get_child_value(i);

        // Because of how flags works, the indices are 2^1 i.e: 1,2,4,8,16...
        obj[element.get_string()[0]] = 2 ** i;
      }

      return obj;
    }
  }

  return null;
}
