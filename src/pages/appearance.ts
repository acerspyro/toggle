import Adw from "gi://Adw";
import GObject from "gi://GObject";

export class ToggleAppearancePreferencePage extends Adw.PreferencesPage {

  static {
    GObject.registerClass(
      {
        GTypeName: "ToggleAppearancePreferencePage",
        Template: "resource:///io/gitlab/orowith2os/Toggle/ui/pages/appearance.ui",
      },
      this,
    );
  }

  constructor(params?: Partial<Adw.PreferencesPage.ConstructorProperties>) {
    super(params);
  }
}