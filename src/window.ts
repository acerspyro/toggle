import Adw from "gi://Adw";
import Gio from "gi://Gio";
import GLib from "gi://GLib";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import { TogglePreferencesWindow } from "./preferences.js";
import { ToggleWarningWindow } from "./warning.js";
import { Settings } from "./application.js";

// make sure GObject register these classes before proceeding
TogglePreferencesWindow;
ToggleWarningWindow;

export class ToggleWindow extends Adw.ApplicationWindow {
  private _preferences!: TogglePreferencesWindow;
  private _stack!: Gtk.Stack;
  private _toastOverlay!: Adw.ToastOverlay;
  private _warning!: ToggleWarningWindow;

  static {
    GObject.registerClass(
      {
        Template: "resource:///io/gitlab/orowith2os/Toggle/ui/window.ui",
        InternalChildren: ["preferences", "toastOverlay", "stack", "warning"],
      },
      this,
    );

    Gtk.Widget.add_shortcut(
      new Gtk.Shortcut({
        action: new Gtk.NamedAction({ action_name: "window.close" }),
        trigger: Gtk.ShortcutTrigger.parse_string("<Control>w"),
      }),
    );
  }

  constructor(params?: Partial<Adw.ApplicationWindow.ConstructorProperties>) {
    super(params);

    const window_size = Settings.get_value("last-window-size");
    const width = window_size.get_child_value(0).get_int32();
    const height = window_size.get_child_value(1).get_int32();
    if (width > 0) this.default_width = width;
    if (height > 0) this.default_height = height;

    const openLink = new Gio.SimpleAction({
      name: "open-link",
      parameter_type: GLib.VariantType.new("s"),
    });

    openLink.connect("activate", (_source, param) => {
      if (param) {
        // When using a variant parameter, we need to get the type we expect
        const link = param.get_string()[0];

        const launcher = new Gtk.UriLauncher({ uri: link });

        /* eslint-disable @typescript-eslint/no-unsafe-call */
        /* eslint-disable @typescript-eslint/no-unsafe-member-access */
        launcher
          .launch(this, null)
          // @ts-expect-error GtkUriLauncher.launch isn't properly generated in our type defs
          .then(() => {
            const toast = new Adw.Toast({
              title: _("Opened link"),
            });
            this._toastOverlay.add_toast(toast);
          })
          .catch(console.error);
      }
    });

    this.add_action(openLink);
  }

  vfunc_close_request() {
    // save window size
    const window_size = GLib.Variant.new("(ii)", [
      this.get_width(),
      this.get_height(),
    ]);

    Settings.set_value("last-window-size", window_size);

    return super.vfunc_close_request();
  }

  // We *always* show the warning.
  private warning_accepted_cb() {
    this._stack.set_visible_child_name("preferences");
  }
}
