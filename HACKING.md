# Getting Started

The easiest way to get started with contributing to Toggle is to utilize [GNOME Builder](flathub.org/apps/org.gnome.Builder).

You may need to run `git submodule update --init --recursive` in order to pull the type declaration submodules. 

If Builder doesn't do it for you automatically, you need to enable the following Flatpak remotes:

- [gnome-nightly](https://wiki.gnome.org/Apps/Nightly)
- [flathub](flatpak.org/setup)

And install the following packages:

- `org.gnome.Platform//master`
- `org.gnome.Sdk//master`
- `org.freedesktop.Sdk.Extension.node18//23.08`
- `org.freedesktop.Sdk.Extension.typescript//23.08`

You can now build and run development builds of Toggle directly from Builder with ease. However, if you need to, you can build Toggle from source manually like so:

1. Install the previously listed packages from the required remotes
2. Install `org.flatpak.Builder` from Flathub
3. In the project root, run `flatpak run --filesystem=$(pwd) org.flatpak.Builder --force-clean build-dir build-aux/io.gitlab.orowith2os.Toggle.Devel.json`
 
You may also want to install and run the package on the user Flatpak installations by adding the following flags: 

- `--user`
- `--install`

Afterwards, you can run the built app (assuming you installed it) with `flatpak run io.gitlab.orowith2os.Toggle.Devel` or by clicking the development app icon in your application drawer.
