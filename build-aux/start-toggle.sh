#!/usr/bin/env bash

IFS=: read -ra host_data_dirs < <(flatpak-spawn --host sh -c 'echo $XDG_DATA_DIRS')

# To avoid potentially muddying up $XDG_DATA_DIRS too much, we link the schema paths
# into a temporary directory within our sandbox.
bridge_dir=$XDG_RUNTIME_DIR/dconf-bridge
mkdir -p "$bridge_dir"

# TODO: make sure it works on non-FHS-conforming systems too, like NixOS. If not,
# should probably just prepend `/run/host` to every path in host_data_dirs. Probably
# not a worry, since the non-`/usr` paths won't actually conflict with the runtime,
# so they just get mounted in as normal directories, given we have `host:ro` permissions.
for dir in "${host_data_dirs[@]}"; do
  if [[ "$dir" == /usr/* ]]; then
    dir=/run/host/"$dir"
  fi

  schemas="$dir/glib-2.0/schemas"
  if [[ -d "$schemas" ]]; then
    bridged=$(mktemp -d XXXXXXXXXX -p "$bridge_dir")
    mkdir -p "$bridged"/glib-2.0
    ln -s "$schemas" "$bridged"/glib-2.0
    XDG_DATA_DIRS=$XDG_DATA_DIRS:"$bridged"
  fi
done

export XDG_DATA_DIRS
exec toggle "$@"
